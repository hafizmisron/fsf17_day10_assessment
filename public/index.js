(function() {

    var AssignmentApp = angular.module("AssignmentApp", [ "ngMessages" ]);

    
    
    //--LoginCtrl-START-------------------------------------------------------------------------
    var LoginCtrl = function($http, $rootScope) {
        loginCtrl = this;

        loginCtrl.buttonPress = function(identifier) {
            console.log("Login Form Button pressed: " + identifier);
            
            // Handle button presses
            switch(identifier) {
                case "submit":

                    var info = {
                        username: loginCtrl.username_lf,
                        password: loginCtrl.password_lf
                    };

                    $http.post("/log-in", info)
                        .then(function() {
                            loginCtrl.message = "Log in successful!";
                        })
                        .catch(function(result){
                            if(result.status == 400) {
                                loginCtrl.message = "User not found";
                            }
                            else if (result.status = 401) {
                                loginCtrl.message = "User not found";
                            }
                            else {
                                loginCtrl.message = "Unknown error"
                            }
                            
                        })
                        
                    break;

                case "clear":
                    clearFields();
                    break;
                
                case "displayForm": {
                    $rootScope.$broadcast("displayForm");
                    break;
                }
            }

            // On load...
            clearFields();

        };

        var clearFields = function() {
            loginCtrl.username_lf = "";
            loginCtrl.password_lf = "";
        };


    };
    //--LoginCtrl-END---------------------------------------------------------------------------



    //--SignupCtrl-START------------------------------------------------------------------------
    var SignupCtrl = function($http, $scope) {
        signupCtrl = this;
        signupCtrl.displayForm = false;

        signupCtrl.buttonPress = function(identifier) {
            console.log("Signup Form Button pressed: " + identifier);

            // Handle button presses
            switch(identifier) {
                case "submit":
                    
                    var info = {
                        fullname    :   signupCtrl.fullname_sf,
                        email       :   signupCtrl.email_sf,
                        username    :   signupCtrl.username_sf,
                        password    :   signupCtrl.password_sf,
                        gender      :   signupCtrl.gender_sf,
                        address1    :   signupCtrl.address1_sf,
                        address2    :   signupCtrl.address2_sf,
                        country     :   signupCtrl.country_sf,
                        contact     :   signupCtrl.contact_sf,
                        dob         :   {
                                            year    :   signupCtrl.dob_year_sf,
                                            month   :   signupCtrl.dob_mth_sf,
                                            day     :   signupCtrl.dob_day_sf
                                        }
                    }

                    $http.post("/sign-up", info)
                        .then(function() {
                            console.log("Data sent successfully");
                            clearFields();
                            signupCtrl.message = "Sign-up successful!";
                            signupCtrl.displayForm = false;
                        });
                    break;

                case "clear":
                    console.log(signupCtrl.dob_year_sf);
                    clearFields();
                    break;
                
                case "hideForm":
                    signupCtrl.displayForm = false;
            }
        }

        signupCtrl.selectedValue = function(type) {
            switch(type) {
                case "year":
                    signupCtrl.dob_mth_ddl_sf = populateDOBDDL("month", "_", signupCtrl.dob_year_sf);
                    signupCtrl.month_disabled = false;
                    break;
                case "month":
                    // Populate the days of the month only when month is already selected
                    signupCtrl.dob_day_ddl_sf = populateDOBDDL("day", signupCtrl.dob_mth_sf, signupCtrl.dob_year_sf)
                    signupCtrl.day_disabled = false;
            } role="alert"
        };

        signupCtrl.passwordCheck = function(password) {
            if(password.length > 7) {
                signupCtrl.password_valid_sf = true;
            }

        }

        var clearFields = function() {
            signupCtrl.message = "";
            signupCtrl.fullname_sf = "";
            signupCtrl.email_sf = "";
            signupCtrl.username_sf = "";
            signupCtrl.password_sf = "";
            signupCtrl.password_valid_sf = false;
            signupCtrl.gender_sf = "";
            signupCtrl.address1_sf = "";
            signupCtrl.address2_sf = "";
            signupCtrl.country_sf = "";
            signupCtrl.contact_sf = "";
            

            // Drop-down list stuff
            signupCtrl.dob_year_sf = "";
            signupCtrl.dob_mth_sf = "";
            signupCtrl.dob_day_sf = "";
            signupCtrl.dob_year_ddl_sf = populateDOBDDL("year");
            signupCtrl.month_disabled = true;
            signupCtrl.day_disabled = true;

            signupCtrl.message = "";

        };

        clearFields();

        $scope.$on("displayForm",function(){
   		    console.log("displayForm");
            signupCtrl.displayForm = true;
	    });
    };
    //--SignupCtrl-END--------------------------------------------------------------------------






    //--Misc Application Functions--------------------------------------------------------------
    
    // Populate Drop-down list...
    // ...restrict user from selecting a date to be earlier than 18 years from today!
    // Also, check if year selected is a leap year.
    var populateDOBDDL = function(type, month, year) {
        var returnVal = [];
        var date = new Date();
        var days = 30;
        var minAge = 18;
        var fullMonths = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

        switch(type) {
            case "month":
                if(year == date.getFullYear() - minAge) {
                    for (var i=0; i<=date.getMonth(); i++)
                        returnVal.push(fullMonths[i]);
                }
                else {
                    returnVal = fullMonths;
                }                
                break;

            case "day":
                // Check month for number of days...and year for leap year
                if(month == "Jan" || 
                   month == "Mar" || 
                   month == "May" || 
                   month == "Jul" || 
                   month == "Aug" || 
                   month == "Oct" || 
                   month == "Dec") {
                    days = 31;
                }
                else if(month == "Feb") {
                    days = 28;
                    if (year % 100 == 0) {
                        if (year % 400 == 0) {
                            days = 29;
                        }
                    }
                    else {
                        if (year % 4 == 0) {
                            days = 29;
                        }
                    }
                }

                if(year == date.getFullYear() - minAge && month == fullMonths[date.getMonth()]) {
                    for(var i=1; i<=date.getDate(); i++) {
                        returnVal.push(i);
                    }
                }
                else {
                    for(var i=1; i<=days; i++) {
                        returnVal.push(i);
                    }
                }

                break;

            case "year":
                for (var i = date.getFullYear() - minAge; i > 1900; i--) {
                    returnVal.push(i);
                }
                break;
        }

        return returnVal;
    };
    //------------------------------------------------------------------------------------------



    AssignmentApp.controller("LoginCtrl", [ "$http", "$rootScope", LoginCtrl ]);
    AssignmentApp.controller("SignupCtrl", [ "$http", "$scope", SignupCtrl ]);
    
})();
// Load Libraries
var express = require("express");
var path = require("path");
var bodyParser = require("body-parser")

// Create Express app
var app = express();

// STORING INFORMATION (Including one sample data)
var userAccounts = [
    { 
        fullname: 'Hafiz Misron',
        email: 'hafiz.misron@gmail.com',
        username: 'hafizmisron',
        password: 'qweR$321',
        gender: 'Male',
        address1: 'Apt Blk 561 Pasir Ris Street 51 #03-269',
        address2: 'S(510561)',
        country: 'Singapore',
        contact: '97719583',
        dob: 
        { 
            year: '1986', 
            month: 'Jun', 
            day: '3' 
        } 
    }
];


// Set port to default: 3000
app.set("port", 3000);

// ROUTES
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, "public")));
app.use("/lib", express.static(path.join(__dirname, "bower_components")));

app.post("/sign-up", function(req, res){
    console.log("Sign up! We're in the server-side now...");
    userAccounts.push(req.body);
    console.log("Added: ");
    console.log(userAccounts[userAccounts.length-1]);
    console.log("Now there are %d records", userAccounts.length);
    for(var i=0; i<userAccounts.length; i++) {
        console.log(userAccounts[i]);
    }
    res.status(202);
    res.end();
});

app.post("/log-in", function(req, res){
    var success = false;
    var info = req.body;
    console.log("Logging in...");
    console.log("Username: " + info.username);
    // Check for username...
    for(var i=0; i<userAccounts.length; i++) {
        if(userAccounts[i].username == info.username) {
            console.log("Found username");
            if(userAccounts[i].password == info.password) {
                console.log("Matched password");
                success = true;
                res.status(202);
            }
            else
                res.status(401); // wrong password
        }
        else
            res.status(400);
    }
    res.end();
});



// Starting the app...
app.listen(app.get("port"), function() {
    console.log("Day 09 app started at %s at port %d.", new Date(), app.get("port"));
});